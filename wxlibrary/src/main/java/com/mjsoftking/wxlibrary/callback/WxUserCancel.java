package com.mjsoftking.wxlibrary.callback;

/**
 * 微信登录、分享、支付 失败时检测到用户取消的回调接口
 */
public interface WxUserCancel {
    void userCancel();
}
