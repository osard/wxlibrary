package com.mjsoftking.wxlibrary.callback;

/**
 * 检测到微信未安装时的回调接口
 */
public interface WxNoInstalled {

    void noInstalled();

}
